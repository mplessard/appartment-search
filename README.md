# apartment_search

Apartment search is a program developped to sort some kijiji's apartment ads by metro station and walk distance.

## Requirements

Install:

- Python3
- sqlite
- pip

then:

```
$ pip install -r requirements.txt
```

## How to start

```
$ ./app.py
# or
$ ./app.py MIN_PRICE MAX_PRICE
# or
$ .app.py MIN_PRICE MAX_PRICE MAX_PAGE
```

## Contributors

- Marie-Pier Lessard

## Status

Under construction.
