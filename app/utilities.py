""" Apartment search is a program developed to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""


import json
import re

import requests

from app.sqlalchemy_db import NearbyStation, Station, session, add_and_commit

with open('api_key.txt', 'r') as file:
    API_KEY = file.read().strip()


def get_google_coordinates(address):
    request = requests.get('http://maps.google.com/maps/api/geocode/json?address={}'.format(address))

    data = json.loads(request.text)

    if data['status'] == 'OK':
        latitude = data['results'][0]['geometry']['location']['lat']
        longitude = data['results'][0]['geometry']['location']['lng']
    elif data['status'] == 'NO_RESULTS':
        zip_code = re.search('[a-zA-Z][0-9][a-zA-Z] ?[0-9][a-zA-Z][0-9]', address)

        if zip_code:
            get_google_coordinates('{}, Montréal, Canada'.format(zip_code.group(0)))
        else:
            return None, None
    elif data['status'] == 'OVER_QUERY_LIMIT':
        print(data['status'])
        exit(0)
    else:
        return None, None

    return latitude, longitude


def find_nearby_station(ad):
    url = ('https://maps.googleapis.com/maps/api/place/nearbysearch/json?key={}'
           '&location={},{}&rankby=distance&types=subway_station'
            .format(API_KEY, ad.latitude, ad.longitude))

    request = requests.get(url)
    data = json.loads(request.text)

    if data['status'] == 'OK':
        counter = 0

        for result in data['results']:
            if counter <= 4:
                add_nearby_station(ad, result['name'])
                counter += 1
            else:
                break

    find_apartment_to_station_time(ad)


def add_nearby_station(ad, station_name):
    station_name = correct_station_name(station_name)

    station = session.query(Station).filter(Station.name == station_name).one_or_none()

    if station is not None:
        already_exists = session.query(NearbyStation).filter(NearbyStation.ad_id == ad.id)\
                        .filter(NearbyStation.station_id == station.id).one_or_none()

        if already_exists is None:
            nearby_station = NearbyStation(ad_id=ad.id, station_id=station.id)

            add_and_commit(nearby_station)


def find_apartment_to_station_time(ad):
    for station in ad.nearby_stations:
        url = ('https://maps.googleapis.com/maps/api/distancematrix/json?key={}&'
               'origins={},{}&destinations=station {} Montreal&mode=walking'.format(API_KEY, ad.latitude, ad.longitude, station.station.name))
        request = requests.get(url)

        if request.status_code != 404:
            data = json.loads(request.text)
            try:
                if data['status'] == 'OK':
                    station.walk_time = int(data['rows'][0]['elements'][0]['duration']['value'])
                else:
                    station.walk_time = None

                session.commit()
            except KeyError:
                print(data)



def correct_station_name(name):
    if 'Michel' in name:
        name = 'Saint-Michel'
    elif 'Assomption' in name:
        name = 'Assomption'
    elif 'Jean-Talon' in name:
        name = 'Jean-Talon'
    elif 'Place-des-Arts' in name:
        name = 'Place-des-Arts'
    elif 'Square-Victoria' in name:
        name = 'Square-Victoria–OACI'
    elif 'Place Saint-Henri' in name:
        name = 'Place-Saint-Henri'
    elif 'Lionel Groulx' in name:
        name = 'Lionel-Groulx'
    elif 'Iberville' in name:
        name = 'D\'Iberville'
    elif 'Park' in name:
        name = 'parc'
    elif 'Édouard Montpetit' in name:
        name = 'Édouard-Montpetit'
    elif 'Montreal University Station' in name:
        name = 'Université-de-Montréal'
    elif 'Angrignon' in name:
        name = 'Angrignon'
    elif 'Jean-Drapeau' in name:
        name = 'Jean-Drapeau'
    elif 'Longueuil' in name:
        name = 'Longueuil'

    return name
